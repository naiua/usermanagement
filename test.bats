#!/usr/bin/env bats

setup() {
  mkdir test_dir
  source ./userManagement
  cat <<_EOT_ >test_dir/unique1.txt
aaa
bbb
ccc
_EOT_
  cat <<_EOT_ >test_dir/unique2.txt
User,Password
aaa,bbb
bbb
ccc
_EOT_
  cat <<_EOT_ >test_dir/notUnique.txt
aaa
bbb
aaa
_EOT_
  cat <<_EOT_ >test_dir/blank.txt
aaa

bbb
_EOT_
}

teardown() {
  rm -r test_dir
}

@test "addition using bc" {
  result="$(echo 2+2 | bc)"
  [ "$result" -eq 4 ]
}

@test "check duplicated" {
    run checkDuplicate test_dir/notUnique.txt
    [ "${status}" -ne 0 ]
}

@test "check not duplicated1" {
    run checkDuplicate test_dir/unique1.txt
    [ "${status}" -eq 0 ]
}

@test "check not duplicated2" {
    run checkDuplicate test_dir/unique2.txt
    [ "${status}" -eq 0 ]
}

@test "check password generationn" {
    run generatePass
    [ -n "${output}" -a ${#output} -eq 8 ]
}

@test "delete blank line" {
    run deleteBlankLine test_dir/blank.txt
}
